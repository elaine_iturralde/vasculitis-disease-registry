var app = angular.module('starter', ['ionic', 'chart.js', 'starter.controllers']);//, 'starter.services'])

app.service("$pouchDB", ["$rootScope", "$q", function($rootScope, $q) {

    var database;
    var remoteDatabase;
    var changeListener;

    this.setDatabase = function(databaseName) {
        database = new PouchDB(databaseName);
        remoteDatabase = new PouchDB("http://localhost:5984/legit");
    }

    this.startListening = function() {
        changeListener = database.changes({
            live: true,
            include_docs: true
        }).on("change", function(change) {
            if(!change.deleted) {
                $rootScope.$on("$pouchDB:change", function(event, data) {
                    $scope.patients[data.doc._id] = data.doc;
                    $scope.$apply();
                });
            } else {
                $rootScope.$on("$pouchDB:delete", function(event, data) {
                    delete $scope.patients[data.doc._id];
                    $scope.$apply();
                });
            }
        });
    }

    this.stopListening = function() {
        changeListener.cancel();
    }

    this.sync = function(remoteDatabase) {
        database.sync(remoteDatabase, {live: true, retry: true});
    }

    this.save = function(jsonDocument) {
        var deferred = $q.defer();
        if(!jsonDocument._id) {
            database.post(jsonDocument).then(function(response) {
                deferred.resolve(response);
            }).catch(function(error) {
                deferred.reject(error);
            });
        } else {
            database.get(jsonDocument._id).then(function(doc) {
              return database.put({
                "_id": jsonDocument._id,
                "_rev": doc._rev,
                "timestamp": jsonDocument.timestamp,
                "firstname": jsonDocument.firstname, 
                "lastname": jsonDocument.lastname, 
                "sex": jsonDocument.sex,
                "caseno": jsonDocument.caseno,
                "md": jsonDocument.md,
                "institution": jsonDocument.institution,
                "contact": jsonDocument.contact,
                "email": jsonDocument.email,
                "address": jsonDocument.address,
                "status": jsonDocument.status,
                "date_registryentry": jsonDocument.date_registryentry,
                "date_diagnosis": jsonDocument.date_diagnosis,
                "date_onset": jsonDocument.date_onset,
                "diseaseduration": jsonDocument.diseaseduration,
                "chiefcomplaint": jsonDocument.chiefcomplaint,
                "civilstatus": jsonDocument.civilstatus,
                "years_education": jsonDocument.years_education,
                "employment": jsonDocument.employment,
                "smoking": jsonDocument.smoking,
                "alcohol": jsonDocument.alcohol,
                "date_birth": jsonDocument.date_birth,
                "expired": jsonDocument.expired,
                "expired_cause": jsonDocument.expired_cause,
                "family": jsonDocument.family,
                "gluco_rec": jsonDocument.gluco_rec,
                "gluco_currrec": jsonDocument.gluco_currrec,
                "gluco_lasttaken": jsonDocument.gluco_lasttaken,
                "gluco_months": jsonDocument.gluco_months,
                "ivcyclo_rec": jsonDocument.ivcyclo_rec,
                "ivcyclo_currrec": jsonDocument.ivcyclo_currrec,
                "ivcyclo_lasttaken": jsonDocument.ivcyclo_lasttaken,
                "ivcyclo_months": jsonDocument.ivcyclo_months,
                "pocyclo_rec": jsonDocument.pocyclo_rec,
                "pocyclo_currrec": jsonDocument.pocyclo_currrec,
                "pocyclo_lasttaken": jsonDocument.pocyclo_lasttaken,
                "pocyclo_months": jsonDocument.pocyclo_months,
                "aza_rec": jsonDocument.aza_rec,
                "aza_currrec": jsonDocument.aza_currrec,
                "aza_lasttaken": jsonDocument.aza_lasttaken,
                "aza_months": jsonDocument.aza_months,
                "metho_rec": jsonDocument.metho_rec,
                "metho_currrec": jsonDocument.metho_currrec,
                "metho_lasttaken": jsonDocument.metho_lasttaken,
                "metho_months": jsonDocument.metho_months,
                "myco_rec": jsonDocument.myco_rec,
                "myco_currrec": jsonDocument.myco_currrec,
                "myco_lasttaken": jsonDocument.myco_lasttaken,
                "myco_months": jsonDocument.myco_months,
                "cyclo_rec": jsonDocument.cyclo_rec,
                "cyclo_currrec": jsonDocument.cyclo_currrec,
                "cyclo_lasttaken": jsonDocument.cyclo_lasttaken,
                "cyclo_months": jsonDocument.cyclo_months,
                "eta_rec": jsonDocument.eta_rec,
                "eta_currrec": jsonDocument.eta_currrec,
                "eta_lasttaken": jsonDocument.eta_lasttaken, 
                "eta_months": jsonDocument.eta_months,
                "infli_rec": jsonDocument.infli_rec,
                "infli_currrec": jsonDocument.infli_currrec,
                "infli_lasttaken": jsonDocument.infli_lasttaken,
                "infli_months": jsonDocument.infli_months,
                "ada_rec": jsonDocument.ada_rec,
                "ada_currrec": jsonDocument.ada_currrec,
                "ada_lasttaken": jsonDocument.ada_lasttaken,
                "ada_months": jsonDocument.ada_months,
                "ritu_rec": jsonDocument.ritu_rec,
                "ritu_currrec": jsonDocument.ritu_currrec,
                "ritu_lasttaken": jsonDocument.ritu_lasttaken,
                "ritu_months": jsonDocument.ritu_months,
                "plasma_rec": jsonDocument.plasma_rec,
                "plasma_currrec": jsonDocument.plasma_currrec,
                "plasma_lasttaken": jsonDocument.plasma_lasttaken,
                "plasma_months": jsonDocument.plasma_months,
                "tmphd_rec": jsonDocument.tmphd_rec,
                "tmphd_currrec": jsonDocument.tmphd_currrec,
                "tmphd_lasttaken": jsonDocument.tmphd_lasttaken,
                "tmphd_months": jsonDocument.tmphd_months,
                "tmpld_rec": jsonDocument.tmpld_rec,
                "tmpld_currrec": jsonDocument.tmpld_currrec,
                "tmpld_lasttaken": jsonDocument.tmpld_lasttaken,
                "tmpld_months": jsonDocument.tmpld_months,
                "aspld_rec": jsonDocument.aspld_rec,
                "aspld_currrec": jsonDocument.aspld_currrec,
                "aspld_lasttaken": jsonDocument.aspld_months,
                "asphd_rec": jsonDocument.asphd_rec,
                "asphd_currrec": jsonDocument.asphd_currrec,
                "asphd_lasttaken": jsonDocument.asphd_lasttaken,
                "asphd_months": jsonDocument.asphd_months,
                "clopi_rec": jsonDocument.clopi_rec,
                "clopi_currrec": jsonDocument.clopi_currrec,
                "clopi_lasttaken": jsonDocument.clopi_lasttaken,
                "clopi_months": jsonDocument.clopi_months,
                "stat_rec": jsonDocument.stat_rec,
                "stat_currrec": jsonDocument.stat_currrec,
                "stat_lasttaken": jsonDocument.stat_lasttaken,
                "stat_months": jsonDocument.stat_months,
                "oral_rec": jsonDocument.oral_rec,
                "oral_currrec": jsonDocument.oral_currrec,
                "oral_lasttaken": jsonDocument.oral_lasttaken,
                "oral_months": jsonDocument.oral_months,
                "est_rec": jsonDocument.est_rec,
                "est_currrec": jsonDocument.est_currrec,
                "est_lasttaken": jsonDocument.est_lasttaken,
                "est_months": jsonDocument.est_months,
                "other_rec": jsonDocument.other_rec,
                "other_currrec": jsonDocument.other_currrec,
                "other_lasttaken": jsonDocument.other_lasttaken,
                "other_months": jsonDocument.other_months,
                "compli_aneurysm": jsonDocument.compli_aneurysm,
                "compli_stroke": jsonDocument.compli_stroke,
                "compli_limb": jsonDocument.compli_limb,
                "compli_dialysis": jsonDocument.compli_dialysis,
                "compli_others": jsonDocument.compli_others,
                "inva_ptca": jsonDocument.inva_ptca,
                "inva_aneurysm": jsonDocument.inva_aneurysm,
                "inva_ampu": jsonDocument.inva_ampu,
                "inva_others": jsonDocument.inva_others
              });
            }).then(function(response) {
              // handle response
            }).catch(function (err) {
              console.log(err);
            });
        }
        return deferred.promise;
    }

    this.delete = function(documentId, documentRevision) {
        return database.remove(documentId, documentRevision);
    }

    this.get = function(documentId) {
      database.get(documentId, {
        revs_info: true
      }).then(function(doc){
        $rootScope.patient = doc;
        $rootScope.$apply();
      })
    }

    this.changes = function(){
      database.changes({
        since: 0,
        include_docs: true,
        limit: 10,
        descending: true
      }).then(function (changes) {
        changes.results.forEach(function (change) {
          var d = new Date(change.doc.timestamp);
          var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
          var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
          var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
          var formattedTime = hours + ":" + minutes;
          var obj = {
            "firstname": change.doc.firstname,
            "lastname": change.doc.lastname,
            "title": change.id,
            "date": formattedDate,
            "time": formattedTime
          }
          $rootScope.changes.push(obj);
          $rootScope.$apply();
        });
      }).catch(function (err) {
        // handle errors
      });
    }

    this.chartsex = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var male = 0, female = 0, other = 0;
            for(var i=0;i<result.rows.length;i++){
                if(result.rows[i].doc.sex == "Male"){
                  male++;
                }
                else if(result.rows[i].doc.sex == "Female"){
                  female++;
                }
                else if(result.rows[i].doc.sex == "Other"){
                  other++;
                }
            }
            $rootScope.visual.sex.data[0][0] = male;
            $rootScope.visual.sex.data[0][1] = female;
            $rootScope.visual.sex.data[0][2] = other;
            $rootScope.$apply();

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.chartstatus = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var active = 0, lost = 0, expired = 0;
            for(var i=0;i<result.rows.length;i++){
                if(result.rows[i].doc.status == "Active"){
                  active++;
                }
                else if(result.rows[i].doc.status == "Lost to Followup"){
                  lost++;
                }
                else if(result.rows[i].doc.status == "Expired"){
                  expired++;
                }
            }
            $rootScope.visual.status.data[0][0] = active;
            $rootScope.visual.status.data[0][1] = lost;
            $rootScope.visual.status.data[0][2] = expired;
            $rootScope.$apply();

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.chartsmoking = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var no = 0, rare = 0, often = 0;
            for(var i=0;i<result.rows.length;i++){
                if(result.rows[i].doc.smoking == "No"){
                  no++;
                }
                else if(result.rows[i].doc.smoking == "Rare"){
                  rare++;
                }
                else if(result.rows[i].doc.smoking == "Often"){
                  often++;
                }
            }
            $rootScope.visual.smoking.data[0][0] = no;
            $rootScope.visual.smoking.data[0][1] = rare;
            $rootScope.visual.smoking.data[0][2] = often;
            $rootScope.$apply();

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.chartalcohol = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var no = 0, rare = 0, often = 0;
            for(var i=0;i<result.rows.length;i++){
                if(result.rows[i].doc.alcohol == "No"){
                  no++;
                }
                else if(result.rows[i].doc.alcohol == "Rare"){
                  rare++;
                }
                else if(result.rows[i].doc.alcohol == "Often"){
                  often++;
                }
            }
            $rootScope.visual.alcohol.data[0][0] = no;
            $rootScope.visual.alcohol.data[0][1] = rare;
            $rootScope.visual.alcohol.data[0][2] = often;
            $rootScope.$apply();

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.chartemployment = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var no = 0, yes = 0;
            for(var i=0;i<result.rows.length;i++){
                if(result.rows[i].doc.employment == "No Source of Income"){
                  no++;
                }
                else if(result.rows[i].doc.employment == "Gainfully Employed"){
                  yes++;
                }
            }
            $rootScope.visual.employment.data[0][0] = no;
            $rootScope.visual.employment.data[0][1] = yes;
            $rootScope.$apply();
        }).catch(function (err) {
            console.log(err);
        });
    }

    this.chartcivilstatus = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var single = 0, married = 0;
            for(var i=0;i<result.rows.length;i++){
                if(result.rows[i].doc.civilstatus == "Single/Widower/Separated"){
                  single++;
                }
                else if(result.rows[i].doc.civilstatus == "Married/Live-In Partner"){
                  married++;
                }
            }
            $rootScope.visual.civilstatus.data[0][0] = single;
            $rootScope.visual.civilstatus.data[0][1] = married;
            $rootScope.$apply();

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.chartbirthmonth = function(){
      database.allDocs({
            include_docs: true
        }).then(function (result) {
            var jan = 0, feb = 0, mar = 0, april = 0, may = 0, june = 0, july = 0, aug = 0, sept = 0, oct = 0, nov = 0, dec = 0;
            for(var i=0;i<result.rows.length;i++){
                var d = new Date(result.rows[i].doc.timestamp);
                if(d.getMonth() == 0){
                  jan++;
                }
                else if(d.getMonth() == 0){
                  feb++;
                }
                else if(d.getMonth() == 0){
                  mar++;
                }
                else if(d.getMonth() == 0){
                  april++;
                }
                else if(d.getMonth() == 0){
                  may++;
                }
                else if(d.getMonth() == 0){
                  june++;
                }
                else if(d.getMonth() == 0){
                  july++;
                }
                else if(d.getMonth() == 0){
                  aug++;
                }
                else if(d.getMonth() == 0){
                  sept++;
                }
                else if(d.getMonth() == 0){
                  oct++;
                }
                else if(d.getMonth() == 0){
                  nov++;
                }
                else if(d.getMonth() == 0){
                  dec++;
                }

            }
            $rootScope.visual.birth.data[0][0] = jan;
            $rootScope.visual.birth.data[0][1] = feb;
            $rootScope.visual.birth.data[0][2] = mar;
            $rootScope.visual.birth.data[0][3] = april;
            $rootScope.visual.birth.data[0][4] = may;
            $rootScope.visual.birth.data[0][5] = june;
            $rootScope.visual.birth.data[0][6] = july;
            $rootScope.visual.birth.data[0][7] = aug;
            $rootScope.visual.birth.data[0][8] = sept;
            $rootScope.visual.birth.data[0][9] = oct;
            $rootScope.visual.birth.data[0][10] = nov;
            $rootScope.visual.birth.data[0][11] = dec;
            $rootScope.$apply();

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.allDocs = function(){
        database.allDocs({
            include_docs: true
        }).then(function (result) {
            for(var i=0;i<result.rows.length;i++){
                var obj = {
                    "_id": result.rows[i].doc._id,
                    "firstname": result.rows[i].doc.firstname,
                    "lastname": result.rows[i].doc.lastname,
                    "case no": result.rows[i].doc.caseno
                }
                $rootScope.patients.push(obj);
                $rootScope.$apply();
            }

        }).catch(function (err) {
            console.log(err);
        });
    }

    this.destroy = function() {
        database.destroy();
    }

}])

app.run(function($ionicPlatform, $pouchDB) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

    $pouchDB.setDatabase("legit");
    if(ionic.Platform.isAndroid()) {
        $pouchDB.sync("http://localhost:5984/legit");
    } else {
        $pouchDB.sync("http://localhost:5984/legit");
    }
})

//lagay sa services
app.service('typesService', function($q) {
  return{
    types: [
        { 
          title: 'Behcet’s Disease', 
          symptom: 
          [
            {
              name: 'Mouth ulcers over past 12 month (3 or more episodes)',
              checked: false,
            },
            {
              name: 'Genital ulcers',
              checked: false,
            },
            {
              name: 'Eye inflammation (anterior uveitis (AU), posterior uveitis (PU), and retinal vasculitis (RV))',
              checked: false,
            },
            { 
              name: 'Skin lesions (pseudofolliculitis; erythema nodosum)',
              checked: false,
            },
            {
              name: 'Genital aphthosis',
              checked: false,
            },
            {
              name: 'Eye lesions',
              checked: false,
            },
            {
              name: 'Pathergy',
              checked: false,
            },
            {
              name: 'Vascular: superficial phlebitis, deep vein thrombosis, large vein thrombosis, arterial thrombosis,  aneurysm',
              checked: false,
            },
          ],
        },
        { 
           title: 'Giant Cell Arteritis', 
           symptom: 
           [
            {
              name: 'Age at disease onset >=50 years',
              checked: false,
            },
            {
              name: 'New headache',
              checked: false,
            },
            {
              name: 'Temporal artery tenderness to palpation or decreased pulsation (not arteriosclerosis)',
              checked: false,
            },
            { 
              name: 'Elevated ESR (>/= 50)',
              checked: false,
            },
            {
              name: 'Abnormal biopsy (artery with vasculitis-- predominance of mononuclear cell infiltration or granulomatous inflamm)',
              checked: false,
            },
          ],
        },
        { 
          title: 'Eosinophilic Granulomatosis with Polyangiitis or Churg Strauss Syndrome', 
          symptom: 
           [
            {
              name: 'Asthma',
              checked: false,
            },
            {
              name: 'Eosinophilia >10%',
              checked: false,
            },
            {
              name: 'Neuropathy, mono or poly',
              checked: false,
            },
            { 
              name: 'Pulmonary infiltrates, non-fixed',
              checked: false,
            },
            {
              name: 'Paranasal sinus abnormality',
              checked: false,
            },
            {
              name: 'Extravascular eosinophils',
              checked: false,
            },
          ],
        },
        {
          title: 'Microscopic Polyangiitis',
          symptom: 
          [
            {
              name: 'p-ANCA', 
              checked: false,
            },
            {
              name:'Fatigue',
              checked: false,
            },
            {
              name:'Fever',
              checked: false,
            },
            {
              name:'Arthrlgia',
              checked: false,
            },
            {
              name:'Abdominal pain',
              checked: false,
            },
            {
              name:'HPN',
              checked: false,
            },
            {
              name:'Renal insufficiency',
              checked: false,
            },
            {
              name:'Neurologic dysfunction',
              checked: false,
            },
          ],
        },
        {
          title: 'Polyarteritis Nodosa',
          symptom: 
          [
            {
              name: 'Weight loss >/=4kg',
              checked: false,
            }, 
            {
              name: 'Livedo Reticularis',
              checked: false,
            },
            {
              name: 'Testicular pain/tenderness',
              checked: false,
            },
            {
              name: 'Myalgia, weakness, leg tenderness',
              checked: false,
            },
            {
              name: 'Mononeuropathy or poly-',
              checked: false,
            },
            {
              name: 'DBP >90mmHg',
              checked: false,
            },
            {
              name: 'BUN >40 mg/dl or creatinine >1.5 mg/dl',
              checked: false,
            },
            {
              name: 'HBsAg or anti-HBs',
              checked: false,
            },
            {
              name: 'Arteriogram: aneurysms/occlusions of the visceral arteries (not arterio; FMD etc)',
              checked: false,
            },
            {
              name: 'Biopsy of small or medium-sized artery with PMN or mononuclean leucocytes',
              checked: false,
            },
          ],
        },
        { title: 'Primary Angiitis of the Central Nervous System',
          symptom: 
          [
            {
              name:'',
              checked:''
            },
          ],
        },
        {title: 'Kawasaki Disease',
          symptom: 
          [
            {
              name: 'Fever',
              checked: false,
            },
            {
              name: 'Bilateral bulbar conjunctival injection',
              checked: false,
            },
            {
              name: 'Peripheral extremity changes (erythema of palms or soles, edema of hands or feet (acute phase), and periungual desquamation (convalescent phase)',
              checked: false,
            },
            {
              name: 'Polymorphous rash',
              checked: false,
            },
            {
              name: 'CLAD (at least 1 lymph node >1.5 cm in diameter)',
              checked: false,
            },
          ],
        },
        {
          title: 'Takayasu’s Arteritis',
          symptom: 
          [
            {
              name: 'Age at Onset of disease =/< 40 years', 
              checked: false,
            },
            {
              name: 'Claudication of extremities: development and worsening of fatigue and discomfort in muscles of 1 or more extremity while in use.',
              checked: false,
            },
            {
              name: 'Decreased brachial artery pulse',
              checked: false,
            },
            {
              name: 'Systolic BP difference greater than 10 mmHg between arms',
              checked: false,
            },
            {
              name: 'Bruit over subclavian arteries or abdominal aorta',
              checked: false,
            },
            {
              name: 'Arteriographic: narrowing/occlusion- aorta/branches; large arteries in  proximal UE/LE (not  arteriosclerosis, FMD..) changes usually focal or segmental',
              checked: false,
            },
            {
              name: 'Obligatory criterion: </= 40 years',
              checked: false,
            },
            {
              name: 'Major: Lesion of the left mid subclavian artery',
              checked: false,
            },
            {
              name: 'Major: Lesion of the right mid subclavian artery',
              checked: false,
            },
            {
              name: 'Minor: High ESR',
              checked: false,
            },
            {
              name: 'Minor: Common carotid artery tenderness',
              checked: false,
            },
            {
              name: 'Minor: Hypertension',
              checked: false,
            },
            {
              name: 'Minor: Aortic regurgitation or annulo-aortic ectasia',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the pulmonary artery',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the left mid common carotid artery',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the distal brachiocephalic trunk',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the thoracic aorta',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the abdominal aorta',
              checked: false,
            },
          ],
        },
        {
          title: 'Granulomatosis with Polyangiitis or Wegener’s Granulomatosis',
          symptom: 
          [
            {
              name: 'Nasal or oral inflammation (painless/painful oral ulcers; purulent/bloody nasal discharge)',
              checked: false,
            },
            {
              name: 'CXR with nodules, fixed infiltrates, or cavities',
              checked: false,
            },
            {
              name: 'Microhematuria (>5 RBCs per HPF) or red cell casts in urine sediment',
              checked: false,
            },
            {
              name: 'Granulomatous inflammation on biopsy',
              checked: false,
            },
          ],
        },
        {
          title: 'Hypersensitivity Vasculitis',
          symptom: 
          [
            {
              name: 'Age >16 yrs at symptom onset',
              checked: false,
            },
            {
              name: 'Medication taken at disease onset',
              checked: false,
            },
            {
              name: 'Palpable non-blanching purpura (not thrombocytopenia-related)',
              checked: false,
            },
            {
              name: 'Maculo-papular rash',
              checked: false,
            },
            {
              name: 'PMN in venule/arteriole wall',
              checked: false,
            },
            {
              name: 'Eosinophils in venule or arteriole at any locations',
              checked: false,
            },
          ],
          
        },
        {
          title: 'Cutaneous Vasculitis',
          symptom: 
          [
            {
              name: '', 
              checked: '',
            },
          ],
        },
        {title: 'Cryoglonulinemia',
          symptom: 
          [
            {
              name: 'Persistently elevated cryocrit, such as >1% for 3-6 months',
              checked: false,
            },
            {
              name: 'PLUS clinical indicators of cryoglobulinemic vasculitis/ thrombosis (LE purpura, particularly w/  leukocytoclastic vasculitis on biopsy or diminished serum C4',
                checked: false,
            },
            {
              name: 'Or PLUS direct evidence of CGs from pathological thrombotic or vasculitic specimens (elution and cryoprecipitation and/or immunofixation)',
              checked: false,
            },
            {
              name: 'Nonspecific: weakness or arthralgia + CG (better if with HCV or Sjogren)',
              checked: false,
            },
          ],
        },
        {
          title: 'Henoch-Schonlein Purpura',
          symptom: 
          [
            {
              name: 'A: Age </= 20 years at onset', 
              checked: false,
            },
            {
              name: 'B: Palpable purpura',
              checked: false,
            },
            {
              name: 'C: Bowel angina (abdominal pain worst after meals)',
              checked: false,
            },
            {
              name: 'D: Granulocytes (extravasculr/perivasc) on biopsy',
              checked: false,
            },
          ],
        }
      ],

      getTypes: function() {
          return this.types
      },
      getType: function(typeTitle) {
          var dfd = $q.defer()
          this.types.forEach(function(type) {
            if (type.title == typeTitle) dfd.resolve(type)
          })

        return dfd.promise
      },
  }
})

app.config(function($ionicConfigProvider){
    $ionicConfigProvider.tabs.position('bottom');
})

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.report', {
    url: '/report',
    views: {
      'menuContent': {
        templateUrl: 'templates/report.html',
        controller: 'textCtrl'
      }
    }
  })

  .state('app.dashboard', {
      url: '/dashboard',
      views: {
        'menuContent': {
          templateUrl: 'templates/dashboard.html',
          controller: 'patientsCtrl'
        }
      }
  })

  .state('app.patientprofile', {
      url: '/patientprofile/:documentId/:documentRevision',
      views: {
        'menuContent': {
          templateUrl: 'templates/patientprofile.html',
          controller: 'patientsCtrl'
        }
      }
  })

  .state('app.addpatient', {
      url: '/addpatient',
      views: {
        'menuContent': {
          templateUrl: 'templates/addpatient.html',
          controller: 'patientsCtrl'
        }
      }
  })

  .state('app.typesofvasculitis', {
      url: '/typesofvasculitis',
      views: {
        'menuContent': {
          templateUrl: 'templates/typesofvasculitis.html',
          controller: 'typesCtrl',
          resolve: {
            types: function(typesService) {
              return typesService.getTypes()
            }
          }
        }
      }
  })

  .state('app.type', {
      url: '/type/:typeTitle',
      views: {
        'menuContent': {
          templateUrl: 'templates/type.html',
          controller: 'typeCtrl',
          resolve: {
            type: function($stateParams, typesService) {
              return typesService.getType($stateParams.typeTitle)
            }
          }
        }
      }
  })

  .state('app.patients', {
      url: '/patients',
      views: {
        'menuContent': {
          templateUrl: 'templates/patients.html',
          controller: 'patientsCtrl'
        }
      }
  })

  .state('app.changesfeed',{
      url:'/changesfeed',
      views: {
        'menuContent': {
          templateUrl: 'templates/changesfeed.html',
          controller: 'patientsCtrl'
        }
      }
  })

  .state('app.diagnosedpatients', {
    url: '/diagnosedpatients',
    views: {
      'menuContent': {
        templateUrl: 'templates/diagnosedpatients.html'
      }
    }
  })
  
  $urlRouterProvider.otherwise('/login');
});
