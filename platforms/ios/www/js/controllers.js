var app = angular.module('starter.controllers', [])

app.controller('typesCtrl', function($scope, types) {
  $scope.types = types;
})

app.controller('typeCtrl', function($scope, type) {
  $scope.type = type;
  // $scope.points = 0;

  // $scope.isChecked = function () {
  //   if(checked == true){
  //     $scope.points = $scope.points + 1;
  //   }
  //   else{
  //     $scope.points = $scope.points - 1;
  //   }
  // }
})

app.controller('reportCtrl', function($scope, $ionicModal) {

  $ionicModal.fromTemplateUrl('templates/report.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
  
  //**ADD TO DATABASE
  // $scope.createContact = function(u) {        
  //   $scope.contacts.push({ name: u.firstName + ' ' + u.lastName });
  //   $scope.modal.hide();
  // };

})

app.controller("hideCtrl", function($scope){
    $scope.show = false;

    $scope.hideCard = function() {
        if($scope.show == true){
          $scope.show = false;
        }
        else{
          $scope.show = true;
        }
    };
})

// app.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

//   // With the new view caching in Ionic, Controllers are only called
//   // when they are recreated or on app start, instead of every page change.
//   // To listen for when this page is active (for example, to refresh data),
//   // listen for the $ionicView.enter event:
//   //$scope.$on('$ionicView.enter', function(e) {
//   //});

//   // Form data for the login modal
//   $scope.loginData = {};

//   // Create the login modal that we will use later
//   $ionicModal.fromTemplateUrl('templates/login.html', {
//     scope: $scope
//   }).then(function(modal) {
//     $scope.modal = modal;
//   });

//   // Triggered in the login modal to close it
//   $scope.closeLogin = function() {
//     $scope.modal.hide();
//   };

//   // Open the login modal
//   $scope.login = function() {
//     $scope.modal.show();
//   };

//   // Perform the login action when the user submits the login form
//   $scope.doLogin = function() {
//     console.log('Doing login', $scope.loginData);

//     // Simulate a login delay. Remove this and replace with your login
//     // code if using a login system
//     $timeout(function() {
//       $scope.closeLogin();
//     }, 1000);
//   };
// })

// app.controller("symptomsCtrl", function($scope){
//   $scope.symptoms =[
//     {title: 'Symptom 1'},
//     {title: 'Symptom 2'},
//     {title: 'Symptom 3'},
//     {title: 'Symptom 4'}
//   ];
// })

// app.controller("diseasesCtrl", function($scope){
//   $scope.diseases =[
//     {title: 'Behcet’s Disease'},
//     {title: 'Giant Cell Arteritis'},
//     {title: 'Eosinophilic Granulomatosis with Polyangiitis or Churg Strauss Syndrome'},
//     {title: 'Microscopic Polyangiitis'},
//     {title: 'Polyarteritis Nodosa'},
//     {title: 'Primary Angiitis of the Central Nervous System'},
//     {title: 'Kawasaki Disease'},
//     {title: 'Takayasu’s Arteritis'},
//     {title: 'Granulomatosis with Polyangiitis or Wegener’s Granulomatosis'},
//     {title: 'Hypersensitivity Vasculitis'},
//     {title: 'Cutaneous Vasculitis'},
//     {title: 'Cryoglonulinemia'},
//     {title: 'Henoch-Schonlein Purpura'}
//   ];
// })

///////////////////////////////////TODO

// app.controller("TodoController", function($scope, $ionicPopup, PouchDBListener) {
//  $scope.todos = [];
//  $scope.create = function() {
//    $ionicPopup.prompt({
//      title: 'Enter a new TODO',
//      inputType: 'text'
//    })
//    .then(function(result) {
//      if(result) {
//        if($scope.hasOwnProperty("todos") !== true) {
//          $scope.todos = [];
//        }
//        localDB.post({title: result, done: false});
//      } else {
//        console.log("Action cancelled.");
//      }
//    });
//  }

//  $scope.update = function(todo) {
//    localDB.put({
//      _id: todo._id,
//      _rev: todo._rev,
//      title: todo.title,
//      done: todo.done
//    })
//    .then(function(result){
//      // You can set some action after the item was updated.
//    });
//  }
//  $scope.$on('add', function(event, todo) {
//    var add = true;
//    angular.forEach($scope.todos, function(value, key) {
//      if (value._id == todo._id) {
//        $scope.todos[key] = todo;
//        add = false;
//        return;
//      }
//    });
//    if (add) {
//      $scope.todos.push(todo);
//    }
//  });

//  $scope.$on('delete', function(event, id) {
//    for(var i = 0; i < $scope.todos.length; i++) {
//      if($scope.todos[i]._id === id) {
//        $scope.todos.splice(i, 1);
//      }
//    }
//  });
// })