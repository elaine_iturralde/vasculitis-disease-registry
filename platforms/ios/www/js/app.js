var app = angular.module('starter', ['ionic', 'starter.controllers']);//, 'starter.services'])

app.service("$pouchDB", ["$rootScope", "$q", function($rootScope, $q) {

    var database;
    var changeListener;

    this.setDatabase = function(databaseName) {
        database = new PouchDB(databaseName);
    }

    this.startListening = function() {
        changeListener = database.changes({
            live: true,
            include_docs: true
        }).on("change", function(change) {
            if(!change.deleted) {
                $rootScope.$broadcast("$pouchDB:change", change);
            } else {
                $rootScope.$broadcast("$pouchDB:delete", change);
            }
        });
    }

    this.stopListening = function() {
        changeListener.cancel();
    }

    this.sync = function(remoteDatabase) {
        database.sync(remoteDatabase, {live: true, retry: true});
    }

    this.save = function(jsonDocument) {
        var deferred = $q.defer();
        if(!jsonDocument._id) {
            database.post(jsonDocument).then(function(response) {
                deferred.resolve(response);
            }).catch(function(error) {
                deferred.reject(error);
            });
        } else {
            database.put(jsonDocument).then(function(response) {
                deferred.resolve(response);
            }).catch(function(error) {
                deferred.reject(error);
            });
        }
        return deferred.promise;
    }

    this.delete = function(documentId, documentRevision) {
        return database.remove(documentId, documentRevision);
    }

    this.get = function(documentId) {
        return database.get(documentId);
    }

    this.destroy = function() {
        database.destroy();
    }

}])

app.run(function($ionicPlatform, $pouchDB) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
    // $pouchDB.setDatabase("nraboy-test");
    // if(ionic.Platform.isAndroid()) {
    //     $pouchDB.sync("http://127.0.0.1:8100/test-database");
    // } else {
    //     $pouchDB.sync("http://localhost:8100/test-database");
    // }
})

//lagay sa services
app.service('typesService', function($q) {
  return{
    types: [
        { 
          title: 'Behcet’s Disease', 
          symptom: 
          [
            {
              name: 'Mouth ulcers over past 12 month (3 or more episodes)',
              checked: false,
            },
            {
              name: 'Genital ulcers',
              checked: false,
            },
            {
              name: 'Eye inflammation (anterior uveitis (AU), posterior uveitis (PU), and retinal vasculitis (RV))',
              checked: false,
            },
            { 
              name: 'Skin lesions (pseudofolliculitis; erythema nodosum)',
              checked: false,
            },
            {
              name: 'Genital aphthosis',
              checked: false,
            },
            {
              name: 'Eye lesions',
              checked: false,
            },
            {
              name: 'Pathergy',
              checked: false,
            },
            {
              name: 'Vascular: superficial phlebitis, deep vein thrombosis, large vein thrombosis, arterial thrombosis,  aneurysm',
              checked: false,
            },
          ],
        },
        { 
           title: 'Giant Cell Arteritis', 
           symptom: 
           [
            {
              name: 'Age at disease onset >=50 years',
              checked: false,
            },
            {
              name: 'New headache',
              checked: false,
            },
            {
              name: 'Temporal artery tenderness to palpation or decreased pulsation (not arteriosclerosis)',
              checked: false,
            },
            { 
              name: 'Elevated ESR (>/= 50)',
              checked: false,
            },
            {
              name: 'Abnormal biopsy (artery with vasculitis-- predominance of mononuclear cell infiltration or granulomatous inflamm)',
              checked: false,
            },
          ],
        },
        { 
          title: 'Eosinophilic Granulomatosis with Polyangiitis or Churg Strauss Syndrome', 
          symptom: 
           [
            {
              name: 'Asthma',
              checked: false,
            },
            {
              name: 'Eosinophilia >10%',
              checked: false,
            },
            {
              name: 'Neuropathy, mono or poly',
              checked: false,
            },
            { 
              name: 'Pulmonary infiltrates, non-fixed',
              checked: false,
            },
            {
              name: 'Paranasal sinus abnormality',
              checked: false,
            },
            {
              name: 'Extravascular eosinophils',
              checked: false,
            },
          ],
        },
        {
          title: 'Microscopic Polyangiitis',
          symptom: 
          [
            {
              name: 'p-ANCA', 
              checked: false,
            },
            {
              name:'Fatigue',
              checked: false,
            },
            {
              name:'Fever',
              checked: false,
            },
            {
              name:'Arthrlgia',
              checked: false,
            },
            {
              name:'Abdominal pain',
              checked: false,
            },
            {
              name:'HPN',
              checked: false,
            },
            {
              name:'Renal insufficiency',
              checked: false,
            },
            {
              name:'Neurologic dysfunction',
              checked: false,
            },
          ],
        },
        {
          title: 'Polyarteritis Nodosa',
          symptom: 
          [
            {
              name: 'Weight loss >/=4kg',
              checked: false,
            }, 
            {
              name: 'Livedo Reticularis',
              checked: false,
            },
            {
              name: 'Testicular pain/tenderness',
              checked: false,
            },
            {
              name: 'Myalgia, weakness, leg tenderness',
              checked: false,
            },
            {
              name: 'Mononeuropathy or poly-',
              checked: false,
            },
            {
              name: 'DBP >90mmHg',
              checked: false,
            },
            {
              name: 'BUN >40 mg/dl or creatinine >1.5 mg/dl',
              checked: false,
            },
            {
              name: 'HBsAg or anti-HBs',
              checked: false,
            },
            {
              name: 'Arteriogram: aneurysms/occlusions of the visceral arteries (not arterio; FMD etc)',
              checked: false,
            },
            {
              name: 'Biopsy of small or medium-sized artery with PMN or mononuclean leucocytes',
              checked: false,
            },
          ],
        },
        { title: 'Primary Angiitis of the Central Nervous System',
          symptom: 
          [
            {
              name:'',
              checked:''
            },
          ],
        },
        {title: 'Kawasaki Disease',
          symptom: 
          [
            {
              name: 'Fever',
              checked: false,
            },
            {
              name: 'Bilateral bulbar conjunctival injection',
              checked: false,
            },
            {
              name: 'Peripheral extremity changes (erythema of palms or soles, edema of hands or feet (acute phase), and periungual desquamation (convalescent phase)',
              checked: false,
            },
            {
              name: 'Polymorphous rash',
              checked: false,
            },
            {
              name: 'CLAD (at least 1 lymph node >1.5 cm in diameter)',
              checked: false,
            },
          ],
        },
        {
          title: 'Takayasu’s Arteritis',
          symptom: 
          [
            {
              name: 'Age at Onset of disease =/< 40 years', 
              checked: false,
            },
            {
              name: 'Claudication of extremities: development and worsening of fatigue and discomfort in muscles of 1 or more extremity while in use.',
              checked: false,
            },
            {
              name: 'Decreased brachial artery pulse',
              checked: false,
            },
            {
              name: 'Systolic BP difference greater than 10 mmHg between arms',
              checked: false,
            },
            {
              name: 'Bruit over subclavian arteries or abdominal aorta',
              checked: false,
            },
            {
              name: 'Arteriographic: narrowing/occlusion- aorta/branches; large arteries in  proximal UE/LE (not  arteriosclerosis, FMD..) changes usually focal or segmental',
              checked: false,
            },
            {
              name: 'Obligatory criterion: </= 40 years',
              checked: false,
            },
            {
              name: 'Major: Lesion of the left mid subclavian artery',
              checked: false,
            },
            {
              name: 'Major: Lesion of the right mid subclavian artery',
              checked: false,
            },
            {
              name: 'Minor: High ESR',
              checked: false,
            },
            {
              name: 'Minor: Common carotid artery tenderness',
              checked: false,
            },
            {
              name: 'Minor: Hypertension',
              checked: false,
            },
            {
              name: 'Minor: Aortic regurgitation or annulo-aortic ectasia',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the pulmonary artery',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the left mid common carotid artery',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the distal brachiocephalic trunk',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the thoracic aorta',
              checked: false,
            },
            {
              name: 'Minor: Lesions of the abdominal aorta',
              checked: false,
            },
          ],
        },
        {
          title: 'Granulomatosis with Polyangiitis or Wegener’s Granulomatosis',
          symptom: 
          [
            {
              name: 'Nasal or oral inflammation (painless/painful oral ulcers; purulent/bloody nasal discharge)',
              checked: false,
            },
            {
              name: 'CXR with nodules, fixed infiltrates, or cavities',
              checked: false,
            },
            {
              name: 'Microhematuria (>5 RBCs per HPF) or red cell casts in urine sediment',
              checked: false,
            },
            {
              name: 'Granulomatous inflammation on biopsy',
              checked: false,
            },
          ],
        },
        {
          title: 'Hypersensitivity Vasculitis',
          symptom: 
          [
            {
              name: 'Age >16 yrs at symptom onset',
              checked: false,
            },
            {
              name: 'Medication taken at disease onset',
              checked: false,
            },
            {
              name: 'Palpable non-blanching purpura (not thrombocytopenia-related)',
              checked: false,
            },
            {
              name: 'Maculo-papular rash',
              checked: false,
            },
            {
              name: 'PMN in venule/arteriole wall',
              checked: false,
            },
            {
              name: 'Eosinophils in venule or arteriole at any locations',
              checked: false,
            },
          ],
          
        },
        {
          title: 'Cutaneous Vasculitis',
          symptom: 
          [
            {
              name: '', 
              checked: '',
            },
          ],
        },
        {title: 'Cryoglonulinemia',
          symptom: 
          [
            {
              name: 'Persistently elevated cryocrit, such as >1% for 3-6 months',
              checked: false,
            },
            {
              name: 'PLUS clinical indicators of cryoglobulinemic vasculitis/ thrombosis (LE purpura, particularly w/  leukocytoclastic vasculitis on biopsy or diminished serum C4',
                checked: false,
            },
            {
              name: 'Or PLUS direct evidence of CGs from pathological thrombotic or vasculitic specimens (elution and cryoprecipitation and/or immunofixation)',
              checked: false,
            },
            {
              name: 'Nonspecific: weakness or arthralgia + CG (better if with HCV or Sjogren)',
              checked: false,
            },
          ],
        },
        {
          title: 'Henoch-Schonlein Purpura',
          symptom: 
          [
            {
              name: 'A: Age </= 20 years at onset', 
              checked: false,
            },
            {
              name: 'B: Palpable purpura',
              checked: false,
            },
            {
              name: 'C: Bowel angina (abdominal pain worst after meals)',
              checked: false,
            },
            {
              name: 'D: Granulocytes (extravasculr/perivasc) on biopsy',
              checked: false,
            },
          ],
        }
      ],

      getTypes: function() {
          return this.types
      },
      getType: function(typeTitle) {
          var dfd = $q.defer()
          this.types.forEach(function(type) {
            if (type.title == typeTitle) dfd.resolve(type)
          })

        return dfd.promise
      },
  }
})

app.config(function($ionicConfigProvider){
    $ionicConfigProvider.tabs.position('bottom');
})

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html'
    })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.report', {
    url: '/report',
    views: {
      'menuContent': {
        templateUrl: 'templates/report.html'
      }
    }
  })

  .state('app.dashboard', {
      url: '/dashboard',
      views: {
        'menuContent': {
          templateUrl: 'templates/dashboard.html'
        }
      }
  })

  .state('app.patientprofile', {
      url: '/patientprofile',
      views: {
        'menuContent': {
          templateUrl: 'templates/patientprofile.html'
        }
      }
  })

  .state('app.addpatient', {
      url: '/addpatient',
      views: {
        'menuContent': {
          templateUrl: 'templates/addpatient.html',
        }
      }
  })

  .state('app.typesofvasculitis', {
      url: '/typesofvasculitis',
      views: {
        'menuContent': {
          templateUrl: 'templates/typesofvasculitis.html',
          controller: 'typesCtrl',
          resolve: {
            types: function(typesService) {
              return typesService.getTypes()
            }
          }
        }
      }
  })

  .state('app.type', {
      url: '/type/:typeTitle',
      views: {
        'menuContent': {
          templateUrl: 'templates/type.html',
          controller: 'typeCtrl',
          resolve: {
            type: function($stateParams, typesService) {
              return typesService.getType($stateParams.typeTitle)
            }
          }
        }
      }
  })

  .state('app.updateprofile', {
      url: '/updateprofile',
      views: {
        'menuContent': {
          templateUrl: 'templates/updateprofile.html'
        }
      }
  })

  .state('app.patients', {
      url: '/patients',
      views: {
        'menuContent': {
          templateUrl: 'templates/patients.html'
        }
      }
  })

  .state('app.diagnosedpatients', {
    url: '/diagnosedpatients',
    views: {
      'menuContent': {
        templateUrl: 'templates/diagnosedpatients.html'
      }
    }
  })
  
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
