BEGIN TRANSACTION;
CREATE TABLE "wegeners" (
	`patient_id`	INTEGER,
	`gpa_nasaloralinflammation`	INTEGER,
	`gpa_cxrwithnodules`	INTEGER,
	`gpa_microhematuria`	INTEGER,
	`gpa_granulomatousinflammation`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "takayasu" (
	`patient_id`	INTEGER,
	`takayasu_ageatonset`	INTEGER,
	`takayasu_claudicationofextremeties`	INTEGER,
	`takayasu_decreasedbrachialarterypulse`	INTEGER,
	`takayasu_systolicbpdifference`	INTEGER,
	`takayasu_bruit`	INTEGER,
	`takayasu_arteriographic`	INTEGER,
	`takayasu_obligatorycriterion`	INTEGER,
	`takayasu_major_leftmidsubclavianartery`	INTEGER,
	`takayasu_major_rightmidsubclavianartery`	INTEGER,
	`takayasu_minor_highesr`	INTEGER,
	`takayasu_minor_commoncarotidarterytenderness`	INTEGER,
	`takayasu_minor_hypertension`	INTEGER,
	`takayasu_minor_aorticregurgitation`	INTEGER,
	`takayasu_minor_lesionsofpulmonaryartery`	INTEGER,
	`takayasu_minor_lesionsofleftmidcommoncarotidartery`	INTEGER,
	`takayasu_minor_lesionsofdistalbrachiocephalictrunk`	INTEGER,
	`takayasu_minor_lesionsofthoracicaorta`	INTEGER,
	`takayasu_minor_lesionsofabdominalaorta`	INTEGER,
	`Field20`	INTEGER
);
CREATE TABLE `systemic_involvement` (
	`patient_id`	INTEGER,
	`systemic_cvs`	INTEGER,
	`systemic_pulmo`	INTEGER,
	`systemic_gi`	INTEGER,
	`systemic_joint`	INTEGER,
	`systemic_renal`	INTEGER,
	`systemic_cns`	INTEGER,
	`systemic_endo`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "social_history" (
	`patient_id`	INTEGER,
	`social_family_member`	INTEGER,
	`social_smoking`	INTEGER,
	`social_alcohol`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `skin_changes` (
	`patient_id`	INTEGER,
	`skin_rash`	INTEGER,
	`skin_raynauds`	INTEGER,
	`skin_erythemanodosum`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `primary_angiitis` (
	`patient_id`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `polyarteritis_nodosa` (
	`patient_id`	INTEGER,
	`pan_weightloss`	INTEGER,
	`pan_livedoreticularis`	INTEGER,
	`pan_testicularpain`	INTEGER,
	`pan_myalgia`	INTEGER,
	`pan_mononeuropathy`	INTEGER,
	`pan_dbp`	INTEGER,
	`pan_bun`	INTEGER,
	`pan_hbsag`	INTEGER,
	`pan_arteriogram`	INTEGER,
	`pan_biopsy`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "past_medical_history" (
	`patient_id`	INTEGER,
	`past_hpn`	INTEGER,
	`past_acs`	INTEGER,
	`past_dm`	INTEGER,
	`past_cva`	INTEGER,
	`past_ckd`	INTEGER,
	`past_others`	TEXT,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `ob_history` (
	`patient_id`	INTEGER,
	`ob_g`	INTEGER,
	`ob_p`	INTEGER,
	`ob_obstetricscore`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "microscopic_polyangiitis" (
	`patient_id`	INTEGER,
	`mpa_panaca`	INTEGER,
	`mpa_fatigue`	INTEGER,
	`mpa_fever`	INTEGER,
	`mpa_arthrigia`	INTEGER,
	`mpa_abdominalpain`	INTEGER,
	`mpa_hpn`	INTEGER,
	`mpa_renalinsufficiency`	INTEGER,
	`mpa_neurologicdysfunction`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `medical_management` (
	`patient_id`	INTEGER,
	`medical_prednisone`	INTEGER,
	`medical_synthetic`	INTEGER,
	`medical_biologics`	INTEGER,
	`medical_cardiacmeds`	TEXT,
	`medical_others`	TEXT,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `invasive_management` (
	`patient_id`	INTEGER,
	`invasive_ptca`	INTEGER,
	`invasive_aneurysmgrafting`	INTEGER,
	`invasive_amputation`	INTEGER,
	`invasive_others`	TEXT,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `complications` (
	`patient_id`	INTEGER,
	`comp_aneurysmrupture`	INTEGER,
	`comp_stroke`	INTEGER,
	`comp_limbischemia`	INTEGER,
	`comp_dialysis`	INTEGER,
	`comp_others`	TEXT,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "main" (
	`main_tablename`	TEXT,
	`main_tableid`	INTEGER
);
CREATE TABLE "kawasaki_disease" (
	`patient_id`	INTEGER,
	`kd_fever`	INTEGER,
	`kd_bilateralbulbar`	INTEGER,
	`kd_peripheralextremitychanges`	INTEGER,
	`kd_polymorphousrash`	INTEGER,
	`kd_clad`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `musculoskeletal_changes` (
	`patient_id`	INTEGER,
	`muscul_arthritis`	INTEGER,
	`muscul_arthralgia`	INTEGER,
	`muscul_limitedmobility`	INTEGER,
	`muscul_myalgia`	INTEGER,
	`muscul_deformity`	INTEGER,
	`muscul_atrophy`	INTEGER,
	`muscul_effusion`	INTEGER,
	`muscul_contracture`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `imaging` (
	`patient_id`	INTEGER,
	`imaging_ecg`	INTEGER,
	`imaging_2decho`	INTEGER,
	`imaging_ultrasound`	INTEGER,
	`imaging_ct`	INTEGER,
	`imaging_mri`	INTEGER,
	`imaging_mra`	INTEGER,
	`imaging_angiogram`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "hypersensitivity_vasculitis" (
	`patient_id`	INTEGER,
	`hsv_ageofsymptomonset`	INTEGER,
	`hsv_medicationtakenatdiseaseonset`	INTEGER,
	`hsv_palablenonblanchingpurpura`	INTEGER,
	`hsv_maculopapularrash`	INTEGER,
	`hsv_pmn`	INTEGER,
	`hsv_eosinophilisinvenule`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "henoch_schonlein" (
	`patient_id`	INTEGER,
	`hsp_age`	INTEGER,
	`hsp_palpablepurpura`	INTEGER,
	`hsp_bowelangina`	INTEGER,
	`hsp_granulocytes`	INTEGER,
	`hsp_a+b`	INTEGER,
	`hsp_b+d`	INTEGER,
	`hsp_a+d`	INTEGER,
	`hsp_c`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `giant_cell` (
	`patient_id`	INTEGER,
	`gca_ageatdisease`	INTEGER,
	`gca_newheadache`	INTEGER,
	`gca_decreasedpulsation`	INTEGER,
	`gca_elevatedesr`	INTEGER,
	`gca_abnormalbiopsy`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "general" (
	`disease_id`	INTEGER,
	`patient_id`	INTEGER,
	PRIMARY KEY(disease_id,patient_id)
);
CREATE TABLE "eosinophilic_gpa" (
	`patient_id`	INTEGER,
	`egpa_asthma`	INTEGER,
	`egpa_eosinophilia`	INTEGER,
	`egpa_neuropathy`	INTEGER,
	`egpa_pulmonaryinfiltrates`	INTEGER,
	`egpa_paranasalsinusabnormality`	INTEGER,
	`egpa_extravasculareosinophils`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "diseases" (
	`disease_code`	TEXT,
	`disease_name`	TEXT,
	`disease_id`	INTEGER PRIMARY KEY AUTOINCREMENT
);
CREATE TABLE "diagnoses" (
	`patient_id`	INTEGER,
	`diagnosis_age_registryentry`	INTEGER,
	`diagnosis_age_diagnosis`	INTEGER,
	`diagnosis_age_symptomonset`	INTEGER,
	`diagnosis_diseaseduration`	INTEGER,
	`diagnosis_chiefcomplaint`	TEXT,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "demographics" (
	`patient_id`	INTEGER,
	`demo_sex`	INTEGER,
	`demo_caseno`	INTEGER,
	`demo_md`	TEXT,
	`demo_institution`	TEXT,
	`demo_contactemail`	TEXT,
	`demo_address`	TEXT,
	`demo_civil_status`	INTEGER,
	`demo_years_education`	INTEGER,
	`demo_employment`	INTEGER,
	`demo_status`	INTEGER,
	`demo_expired_months`	INTEGER,
	`demo_expired_cause`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `cutaneous_vasculitis` (
	`patient_id`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "cryoglonulinemia" (
	`patient_id`	INTEGER,
	`cg_elevatedcryocrit`	INTEGER,
	`cg_plusclinicalindicators`	INTEGER,
	`cg_plusdirectevidence`	INTEGER,
	`cg_nonspecific`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "general_symptoms" (
	`patient_id`	INTEGER,
	`gen_weightloss`	INTEGER,
	`gen_weakness`	INTEGER,
	`gen_fatigue`	INTEGER,
	`gen_fever`	INTEGER,
	`gen_anorexia`	INTEGER,
	`gen_chf`	INTEGER,
	`gen_angina`	INTEGER,
	`gen_limb`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `confidential` (
	`patient_id`	INTEGER,
	`patient_key`	INTEGER,
	`patient_name`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "category" (
	`disease_id`	INTEGER,
	`category_name`	INTEGER,
	`field_name`	INTEGER,
	PRIMARY KEY(disease_id)
);
CREATE TABLE "chest_examination" (
	`patient_id`	INTEGER,
	`chest_cardiac_enlargement`	TEXT,
	`chest_murmur`	INTEGER,
	`chest_pulses`	TEXT,
	`chest_rales`	INTEGER,
	`chest_nve`	INTEGER,
	`chest_edema`	INTEGER,
	`chest_gallop`	INTEGER,
	`chest_rub`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "behcets_disease" (
	`patient_id`	INTEGER,
	`bd_mouthulcers`	INTEGER,
	`bd_genitalulcers`	INTEGER,
	`bd_eyeinflammation`	INTEGER,
	`bd_skinlesions`	INTEGER,
	`bd_pathergy`	INTEGER,
	`Field7`	INTEGER,
	`bd_genitalaphthosis`	INTEGER,
	`bd_eyelesions`	INTEGER,
	`bd_vascular`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `baseline_pe` (
	`patient_id`	INTEGER,
	`pe_height`	INTEGER,
	`pe_weight`	INTEGER,
	`pe_bmi`	INTEGER,
	`pe_waist`	INTEGER,
	`pe_sbp`	INTEGER,
	`pe_dbp`	INTEGER,
	`pe_hr`	INTEGER,
	`pe_oxygen`	INTEGER,
	`pe_6mw`	INTEGER,
	`pe_nyfc`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE `baseline_labresults` (
	`patient_id`	INTEGER,
	`lab_hemoglobin`	INTEGER,
	`lab_wbc`	INTEGER,
	`lab_neutrophils`	INTEGER,
	`lab_lymphocytes`	INTEGER,
	`lab_eosinophils`	INTEGER,
	`lab_platelet`	INTEGER,
	`lab_creatinine`	INTEGER,
	`lab_bun`	INTEGER,
	`lab_proteinuria`	INTEGER,
	`lab_hematuria`	INTEGER,
	`lab_glucosuria`	INTEGER,
	`lab_po2`	INTEGER,
	`lab_pco2`	INTEGER,
	`lab_ana`	INTEGER,
	`lab_c3`	INTEGER,
	`lab_coombs`	INTEGER,
	`lab_antidsdna`	INTEGER,
	`lab_anticentromere`	INTEGER,
	`lab_crp`	TEXT,
	`lab_esr`	INTEGER,
	`lab_rf`	INTEGER,
	`lab_ddimer`	INTEGER,
	`lab_troponin`	INTEGER,
	PRIMARY KEY(patient_id)
);
CREATE TABLE "baseline_echo" (
	`patient_id`	INTEGER,
	`echo_ef`	INTEGER,
	`echo_tapse`	INTEGER,
	`echo_pulmopressure`	INTEGER,
	`echo_valves`	INTEGER,
	`echo_chamber`	TEXT,
	`echo_thrombus`	INTEGER
);
CREATE TABLE `baseline_ecg` (
	`patient_id`	INTEGER,
	`ecg_rhythm`	TEXT,
	`ecg_axis`	TEXT,
	`ecg_chamber`	TEXT,
	`ecg_others`	TEXT,
	PRIMARY KEY(patient_id)
);
COMMIT;
