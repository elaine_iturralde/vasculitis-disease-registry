var app = angular.module('starter', ['ionic', 'chart.js', 'starter.simplecouch', 'starter.controllers', 'starter.services', 'authorization']);

app.run(function($ionicPlatform, $pouchDB) {
    $ionicPlatform.ready(function() {
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });

    $pouchDB.setDatabase("legit");
    //if(ionic.Platform.isAndroid()) {
    // $pouchDB
    // .sync("http://localhost:5984/legit", {live: true, retry: true});
})

app.config(function($ionicConfigProvider){
    $ionicConfigProvider.tabs.position('bottom');
})

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.report', {
    url: '/report',
    views: {
      'menuContent': {
        templateUrl: 'templates/report.html',
        controller: 'textCtrl'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.dashboard', {
    url: '/dashboard',
    views: {
      'menuContent': {
        templateUrl: 'templates/dashboard.html',
        controller: 'patientsCtrl'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.patientprofile', {
    url: '/patientprofile/:documentId/:documentRevision',
    views: {
      'menuContent': {
        templateUrl: 'templates/patientprofile.html',
        controller: 'patientsCtrl'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.addpatient', {
    url: '/addpatient',
    views: {
      'menuContent': {
        templateUrl: 'templates/addpatient.html',
        controller: 'patientsCtrl'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.typesofvasculitis', {
    url: '/typesofvasculitis',
    views: {
      'menuContent': {
        templateUrl: 'templates/typesofvasculitis.html',
        controller: 'typesCtrl',
        resolve: {
          types: function(typesService) {
            return typesService.getTypes()
          }
        }
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.type', {
    url: '/type/:typeTitle',
    views: {
      'menuContent': {
        templateUrl: 'templates/type.html',
        controller: 'typeCtrl',
        resolve: {
          type: function($stateParams, typesService) {
            return typesService.getType($stateParams.typeTitle)
          }
        }
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.patients', {
    url: '/patients',
    views: {
      'menuContent': {
        templateUrl: 'templates/patients.html',
        controller: 'patientsCtrl'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })

  .state('app.changesfeed',{
    url:'/changesfeed',
    views: {
      'menuContent': {
        templateUrl: 'templates/changesfeed.html',
        controller: 'patientsCtrl'
      }
    },
    data: {
      authorization: true,
      redirectTo: 'login'
    }
  })
  
  $urlRouterProvider.otherwise('login');
});
