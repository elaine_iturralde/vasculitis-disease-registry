
var app = angular.module('starter.controllers', [])

app.controller('patientsCtrl', function($scope, $rootScope, $state, $stateParams, $pouchDB, Authorization, couchdb, $ionicPopup, $ionicHistory) {
  
  $rootScope.patients = [];
  $rootScope.feed = [];
  $rootScope.patient = {};
  $rootScope.visual = {
    "sex": {
      "labels": ["Male", "Female", "Others"],
      "data": [[]]
    },
    "civilstatus": {
      "labels": ["Single/Widower/Separated", "Married/Live-In Partner"],
      "data": [[]]
    },
    ///////////////////////
    "birth": {
      "labels": ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
      "data": [[]]
    },
    ///////////////////////
    "status": {
      "labels": ["Active", "Lost to Followup", "Expired"],
      "data": [[]]
    },
    "employment": {
      "labels": ["No Source of Income", "Gainfully Employed"],
      "data": [[]]
    },
    "smoking": {
      "labels": ["No", "Rare", "Often"],
      "data": [[]]
    },
    "alcohol": {
      "labels": ["No", "Rare", "Often"],
      "data": [[]]
    }
  };
  $rootScope.changes = [];

  $scope.save = function(_id,_rev,timestamp,firstname,lastname,sex,caseno,md,institution,contact,email,address,status,date_registryentry,date_diagnosis,date_onset,diseaseduration,chiefcomplaint,civilstatus,years_education,employment,smoking,alcohol,date_birth,expired,expired_cause,family,gluco_rec,gluco_currrec,gluco_lasttaken,gluco_months,ivcyclo_rec,ivcyclo_currrec,ivcyclo_lasttaken,ivcyclo_months,pocyclo_rec,pocyclo_currrec,pocyclo_lasttaken,pocyclo_months,aza_rec,aza_currrec,aza_lasttaken,aza_months,metho_rec,metho_currrec,metho_lasttaken,metho_months,myco_rec,myco_currrec,myco_lasttaken,myco_months,cyclo_rec,cyclo_currrec,cyclo_lasttaken,cyclo_months,eta_rec,eta_currrec,eta_lasttaken,eta_months,infli_rec,infli_currrec,infli_lasttaken,infli_months,ada_rec,ada_currrec,ada_lasttaken,ada_months,ritu_rec,ritu_currrec,ritu_lasttaken,ritu_months,plasma_rec,plasma_currrec,plasma_lasttaken,plasma_months,tmphd_rec,tmphd_currrec,tmphd_lasttaken,tmphd_months,tmpld_rec,tmpld_currrec,tmpld_lasttaken,tmpld_months,aspld_rec,aspld_currrec,aspld_lasttaken,aspld_months,asphd_rec,asphd_currrec,asphd_lasttaken,asphd_months,clopi_rec,clopi_currrec,clopi_lasttaken,clopi_months,stat_rec,stat_currrec,stat_lasttaken,stat_months,oral_rec,oral_currrec,oral_lasttaken,oral_months,est_rec,est_currrec,est_lasttaken,est_months,other_name,other_rec,other_currrec,other_lasttaken,other_months,compli_aneurysm,compli_stroke,compli_limb,compli_dialysis,compli_others,inva_ptca,inva_aneurysm,inva_ampu,inva_others,changes){
    
    var timestamp = new Date().getTime();

    var patient = { 
      "_id": _id,
      "_rev": _rev,
      "timestamp": timestamp,
      "firstname": firstname, 
      "lastname": lastname, 
      "sex": sex,
      "caseno": caseno,
      "md": md,
      "institution": institution,
      "contact": contact,
      "email": email,
      "address": address,
      "status": status,
      "date_registryentry": date_registryentry,
      "date_diagnosis": date_diagnosis,
      "date_onset": date_onset,
      "diseaseduration": diseaseduration,
      "chiefcomplaint": chiefcomplaint,
      "civilstatus": civilstatus,
      "years_education": years_education,
      "employment": employment,
      "smoking": smoking,
      "alcohol": alcohol,
      "date_birth": date_birth,
      "expired": expired,
      "expired_cause": expired_cause,
      "family": family,
      "gluco_rec": gluco_rec,
      "gluco_currrec": gluco_currrec,
      "gluco_lasttaken": gluco_lasttaken,
      "gluco_months": gluco_months,
      "ivcyclo_rec": ivcyclo_rec,
      "ivcyclo_currrec": ivcyclo_currrec,
      "ivcyclo_lasttaken": ivcyclo_lasttaken,
      "ivcyclo_months": ivcyclo_months,
      "pocyclo_rec": pocyclo_rec,
      "pocyclo_currrec": pocyclo_currrec,
      "pocyclo_lasttaken": pocyclo_lasttaken,
      "pocyclo_months": pocyclo_months,
      "aza_rec": aza_rec,
      "aza_currrec": aza_currrec,
      "aza_lasttaken": aza_lasttaken,
      "aza_months": aza_months,
      "metho_rec": metho_rec,
      "metho_currrec": metho_currrec,
      "metho_lasttaken": metho_lasttaken,
      "metho_months": metho_months,
      "myco_rec": myco_rec,
      "myco_currrec": myco_currrec,
      "myco_lasttaken": myco_lasttaken,
      "myco_months": myco_months,
      "cyclo_rec": cyclo_rec,
      "cyclo_currrec": cyclo_currrec,
      "cyclo_lasttaken": cyclo_lasttaken,
      "cyclo_months": cyclo_months,
      "eta_rec": eta_rec,
      "eta_currrec": eta_currrec,
      "eta_lasttaken": eta_lasttaken, 
      "eta_months": eta_months,
      "infli_rec": infli_rec,
      "infli_currrec": infli_currrec,
      "infli_lasttaken": infli_lasttaken,
      "infli_months": infli_months,
      "ada_rec": ada_rec,
      "ada_currrec": ada_currrec,
      "ada_lasttaken": ada_lasttaken,
      "ada_months": ada_months,
      "ritu_rec": ritu_rec,
      "ritu_currrec": ritu_currrec,
      "ritu_lasttaken": ritu_lasttaken,
      "ritu_months": ritu_months,
      "plasma_rec": plasma_rec,
      "plasma_currrec": plasma_currrec,
      "plasma_lasttaken": plasma_lasttaken,
      "plasma_months": plasma_months,
      "tmphd_rec": tmphd_rec,
      "tmphd_currrec": tmphd_currrec,
      "tmphd_lasttaken": tmphd_lasttaken,
      "tmphd_months": tmphd_months,
      "tmpld_rec": tmpld_rec,
      "tmpld_currrec": tmpld_currrec,
      "tmpld_lasttaken": tmpld_lasttaken,
      "tmpld_months": tmpld_months,
      "aspld_rec": aspld_rec,
      "aspld_currrec": aspld_currrec,
      "aspld_lasttaken": aspld_months,
      "asphd_rec": asphd_rec,
      "asphd_currrec": asphd_currrec,
      "asphd_lasttaken": asphd_lasttaken,
      "asphd_months": asphd_months,
      "clopi_rec": clopi_rec,
      "clopi_currrec": clopi_currrec,
      "clopi_lasttaken": clopi_lasttaken,
      "clopi_months": clopi_months,
      "stat_rec": stat_rec,
      "stat_currrec": stat_currrec,
      "stat_lasttaken": stat_lasttaken,
      "stat_months": stat_months,
      "oral_rec": oral_rec,
      "oral_currrec": oral_currrec,
      "oral_lasttaken": oral_lasttaken,
      "oral_months": oral_months,
      "est_rec": est_rec,
      "est_currrec": est_currrec,
      "est_lasttaken": est_lasttaken,
      "est_months": est_months,
      "other_name": other_name,
      "other_rec": other_rec,
      "other_currrec": other_currrec,
      "other_lasttaken": other_lasttaken,
      "other_months": other_months,
      "compli_aneurysm": compli_aneurysm,
      "compli_stroke": compli_stroke,
      "compli_limb": compli_limb,
      "compli_dialysis": compli_dialysis,
      "compli_others": compli_others,
      "inva_ptca": inva_ptca,
      "inva_aneurysm": inva_aneurysm,
      "inva_ampu": inva_ampu,
      "inva_others": inva_others,
      "changes": changes,
    };
    if($stateParams.documentId) {
        patient["_id"] = $stateParams.documentId;
        patient["_rev"] = $stateParams.documentRevision;
    }
    $pouchDB.save(patient).then(function(doc) {
        console.log(patient);
    });
  }

  $scope.sync = function(username, password){
    couchdb.user.login(username, password, function (admin){
      if(admin.roles[0] == "_admin"){
        couchdb.user.isAuthenticated(function (created){
          $pouchDB.sync(username, password);
        })
        var alertPopup = $ionicPopup.alert({
            title: 'Sync success!',
            template: 'Click cancel to continue.'
        });
      }
      else{
        var alertPopup = $ionicPopup.alert({
            title: 'Sync failed!',
            template: 'Please check your credentials!'
        });
        Authorization.go('app.dashboard');
      }
    })
  }

  $scope.get = function(id){
    $pouchDB.get(id);
  }

  $scope.chart = function(){
    $pouchDB.chartsex();
    $pouchDB.chartcivilstatus();
    $pouchDB.chartbirthmonth();
    $pouchDB.chartstatus();
    $pouchDB.chartemployment();
    $pouchDB.chartsmoking();
    $pouchDB.chartalcohol();
  }

  $scope.allDocs = function(){
    $pouchDB.allDocs();
  }
  
  $scope.delete = function(id, rev){
    $pouchDB.delete(id, rev);
  }

  $scope.feed = function(){
    $pouchDB.changes();
  }
  
  $scope.back = function(){ 
    $ionicHistory.goBack();
  }
})

app.controller('popupCtrl',function($scope, $ionicPopup, $timeout) {

 $scope.showPopup = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'The patient has been added/updated successfully!'
   });
   alertPopup.then(function(res) {
     console.log('Successfully added/updated');
   });
 };
})

app.controller('textCtrl', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
  
  // Load the data
  $http.get('http://www.corsproxy.com/loripsum.net/api/plaintext').then(function (res) {
    $scope.loremIpsum = res.data;
    $timeout(expand, 0);
  });
  
  $scope.autoExpand = function(e) {
        var element = typeof e === 'object' ? e.target : document.getElementById(e);
        var scrollHeight = element.scrollHeight -60; // replace 60 by the sum of padding-top and padding-bottom
        element.style.height =  scrollHeight + "px";    
    };
  
  function expand() {
    $scope.autoExpand('TextArea');
  }
}])

.controller('loginCtrl', function($scope, $state, Authorization, couchdb, $ionicPopup) {
  $scope.onLogin = function(username, password) {
    couchdb.user.login(username, password, function (admin){
      couchdb.user.isAuthenticated(function (created){
        Authorization.go('app.dashboard');
      })
    })
    .catch(function(){
      var alertPopup = $ionicPopup.alert({
            title: 'Login failed!',
            template: 'Please check your credentials!'
        });
        Authorization.go('login');
    })
  }
})

.controller('logoutCtrl', function($scope, $state, Authorization, couchdb){
  $scope.onLogout = function() {
    couchdb.user.logout(function(logout){
      if(logout == true){
        Authorization.go('login');
      }
    })
  }
})

app.controller('typesCtrl', function($scope, types) {
  $scope.types = types;
})

app.controller('typeCtrl', function($scope, type) {
  $scope.type = type;
})

app.controller('reportCtrl', function($scope, $ionicModal) {

  $ionicModal.fromTemplateUrl('templates/report.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.hide();
    window.location.reload(true);
    $hardwareBackButtonClose = true;
  };

})

.controller('syncController', function($scope, $ionicModal) {
  
  $ionicModal.fromTemplateUrl('templates/sync.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    $scope.modal.remove()
    .then(function() {
      $scope.modal = null;
    });
  };
});

app.controller("hideCtrl", function($scope){
    $scope.show = false;

    $scope.hideCard = function() {
        if($scope.show == true){
          $scope.show = false;
        }
        else{
          $scope.show = true;
        }
    };
})
// })